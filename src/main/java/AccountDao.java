import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

public class AccountDao {
    private static final Logger logger = LogManager.getLogger(AccountDao.class);

    public void createAccount(Account account) {
        try (Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/test_task",
                "postgres",
                "0000")) {
            PreparedStatement ps = connection.prepareStatement("insert into accounts (id, money) values (?, ?)");
            ps.setString(1, account.getId());
            ps.setInt(2, account.getMoney());
            ps.executeUpdate();
            logger.info("Создан счет " + account.getId() + " сумма " + account.getMoney());

        } catch (SQLException e) {
            logger.info("Счет не создан!");
            throw new CustomException(e);
        }
    }
}


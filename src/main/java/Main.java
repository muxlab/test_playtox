import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        int numberOfAccounts = 4; // можно поменять
        int numberOfThreads = 2;  // здесь тоже
        Generate generate = new Generate();
        ArrayList<Account> accounts = new ArrayList<>();
        AccountDao accountDao = new AccountDao();
        for (int i = 0; i < numberOfAccounts; i++) {
            Account account = new Account(generate.generateName(), 10000);
            accounts.add(account);
            accountDao.createAccount(account);
        }
        for (int i = 0; i < numberOfThreads; i++) {
            new Thread(new TransactionManadger(accounts, generate)).start();
        }
    }
}

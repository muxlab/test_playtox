import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TransactionDao {
    private static final Logger logger = LogManager.getLogger(TransactionDao.class);

    public void doTransaction(Account from, Account to, int amount) {
        try (Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/test_task",
                "postgres",
                "0000")) {
            try {
                connection.setAutoCommit(false);
                PreparedStatement ps = connection.prepareStatement("UPDATE accounts SET money = money - ?" +
                        " WHERE id = ?");
                ps.setInt(1, amount);
                ps.setString(2, from.getId());
                ps.executeUpdate();
                ps = connection.prepareStatement("UPDATE accounts SET money = money + ?" +
                        " WHERE id = ?");
                ps.setInt(1, amount);
                ps.setString(2, to.getId());
                ps.executeUpdate();
                connection.commit();
                logger.info("Сделан перевод между счетом " + from.getId() +
                        " и счетом " + to.getId() + " на сумму " +
                        amount);
                TransactionManadger.atomicInteger.incrementAndGet();
            } catch (SQLException e) {
                try {
                    if (connection != null) {
                        logger.info("Перевод не прошел. Возможно, нехватает средств на счете");
                        connection.rollback();
                    }
                } catch (SQLException ex) {
                    throw new CustomException(ex);
                }
            }
        } catch (SQLException e) {
            throw new CustomException(e);
        }
    }
}

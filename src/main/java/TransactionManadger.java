import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class TransactionManadger implements Runnable {
    private List<Account> accounts;
    private Generate generate;
    public static AtomicInteger atomicInteger = new AtomicInteger(0);

    public TransactionManadger(List<Account> accounts, Generate generate) {
        this.accounts = accounts;
        this.generate = generate;
    }

    @Override
    public void run() {
        TransactionDao transactionDao = new TransactionDao();
        while (atomicInteger.get() < 30) {
            int numberAccountFrom = generate.generateNumber(0, accounts.size() - 1);
            int numberAccountTo = generate.generateNumber(0, accounts.size() - 1);
            if (numberAccountFrom == numberAccountTo) {
                continue;
            }
            transactionDao.doTransaction(
                    accounts.get(numberAccountFrom),
                    accounts.get(numberAccountTo),
                    generate.generateNumber(0, 10000)
            );
            try {
                Thread.sleep(generate.generateNumber(1000, 2000));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

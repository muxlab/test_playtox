create table accounts
(
    id       VARCHAR(25) PRIMARY KEY,
    money    INTEGER CHECK (accounts.money >= 0)  NOT NULL
);